import React, { Component } from 'react';
import './styles.css'
import Nav from '../component/nav/Nav'
import ListUser from '../component/listuser/ListUser';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <div className="main-home-page">
        <Nav />
        <ListUser />
      </div>
    )
  }
}

export default Home;
