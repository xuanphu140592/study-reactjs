import React, { Component } from 'react';
import './Styles.css'

const listUser = [
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 2, name: 'Phu', address: 'quang nam' },
  { id: 3, name: 'Hung', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Hieu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
  { id: 1, name: 'Phu', address: 'quang nam' },
]

class ListUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      address: '',
      isShowForm: false,
    }
  }

  _renderButtonAdd = (text, bg) => {
    return (
      <button className={`btn-add ${bg}`} onClick={() => this._onToggleAdd()}>{text}</button>
    )
  }

  _renderHeaderTable = () => {
    return (
      <div className="table-header">
        <div>#</div>
        <div>Name</div>
        <div>Address</div>
        <div>Action</div>
      </div>
    )
  }

  _renderBodyTable = (users) => {
    const list = !users && users.length ? [] : users.map((user, index) => {
      return (
        <div className="table-row" key={index.toString()}>
          <div>{(index + 1).toString()}</div>
          <div>{user.name}</div>
          <div>{user.address}</div>
          <div>
            <button className="btn-add bg-primary" onClick={() => this._onToggleEdit(index)}>Edit</button>
            <button className="btn-add bg-danger" onClick={() => this._onToggleDelete(user.id)}>Delete</button>
          </div>
        </div>
      )
    })
    return (<div className="wraper-body-table">{list}</div>)
  }

  _renderFormUser = () => {
    const { name, address } = this.state
    return (
      <div>
        <form>
          <input type="text" value={name} readOnly/>
          <input type="text" value={address} readOnly/>
        </form>
      </div>
    )
  }

  _onToggleDelete = (id) => {
    this.setState({ isShowForm: false })
  }

  _onToggleEdit = (index) => {
    this.setState({ isShowForm: true })
    const { name, address } = listUser[index]
    this.setState({name, address})
  }

  _onToggleAdd = () => {
    this.setState({ isShowForm: true, name: '', address: '' })
  }

  render() {
    const { isShowForm } = this.state
    return (
      <div className="main-list-user">
        <div className="wraper-list-user">
          {this._renderButtonAdd('Add new', 'bg-secondary')}
          {isShowForm && this._renderFormUser()}
          {this._renderHeaderTable()}
          {this._renderBodyTable(listUser)}
        </div>
      </div>
    )
  }
}

export default ListUser;
